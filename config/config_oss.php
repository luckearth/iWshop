<?php

$config->oss = [
    // 是否开启oss
    'on' => false,
    // 挂载点
    'endpoint' => 'oss-cn-shenzhen.aliyuncs.com',
    // accessKeyId
    'accessKeyId' => '',
    // accessKeySecret
    'accessKeySecret' => '',
    // bucket
    'bucket' => '',
    // domain
    'baseroot' => '',
    // 上传目标目录
    'dir' => 'iwshop/'
];