<?php

class AuthGroup extends Model{

    public function getGroups($where=array()){
        $data['list']=db('auth_group')->where($where)->select();
        $data['total']=db('auth_group')->where($where)->count();
        return $data;
    }

    public function addGroup()
    {
        $params=input('post.');
        return db('auth_group')->create($params);
    }

    public function getGroupRuleIds($id)
    {
        $rules=db('auth_group')->where(['id'=>$id])->field('rules')->find();
        return array_filter(explode(',',$rules['rules']));
    }


    public function updateGroupRules($group_id)
    {
        $rules=$this->pPost('rules');
        $params['rules'][]=0;
        return db('auth_group')->where(['id'=>$group_id])->save(['rules'=>implode(',',$rules),'update_time'=>time()]);
    }


    public function editGroup($group_id)
    {
        $params=input('post.');
        $params['update_time']=NOW_TIME;
        return db('auth_group')->where(['id'=>$group_id])->update($params);
    }  



}

