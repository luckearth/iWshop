<?php 
/**
 * 权限管理控制器
 */
class AuthManage extends ControllerAdmin
{   


    public function __construct($ControllerName, $Action, $QueryString)
    {
        parent::__construct($ControllerName, $Action, $QueryString);
    }


	/**
     * 权限分组列表
     * @return mixed
     */
    public function index(){
        $this->loadModel('AuthGroup');
        $this->assign('groups',$this->AuthGroup->getGroups());
        $this->show('wdminpage/authmanage/index.tpl');
    }

    /**
     * 添加组
     */
    public function addGroup()
    {   

        if(isPost()){
            
             if(\app\admin\model\AuthGroup::addGroup()){
                return $this->success('新增成功',url('index'));
             }

             return $this->error('新增失败！'.$Model->getError);   

        }
        

        return $this->fetch('add_edit');
    }



    public function editGroup($group_id)
    {   
        if(isPost()){

             if(\app\admin\model\AuthGroup::editGroup($group_id)){
                return $this->success('更新成功');
             }
             return $this->error('更新失败！'.$Model->getError);   

        }

        $group=db('auth_group')->where(['id'=>$group_id])->find();


        return $this->fetch('add_edit',$group);
    }

    
    /**
     * 权限分配
     * @param  init $group_id 组ID
     */
    public function allotGroupAccess($Q)
    {   
        $this->loadModel('AuthGroup');

        $group_id=$Q->group_id;

        if(isPost()){

            $group_id=$this->pPost('group_id');

            if($this->AuthGroup->updateGroupRules($group_id)){
                return json(1,'更新成功');
            }else{
                return json(0,'更新失败！');
            }
        }
        
        $data['group_id']=$group_id;
        //获取所有节点
        $this->loadModel('AuthRule');
        $rules=$this->AuthRule->getAllRules();

        //获取本组的节点
        $data['group_rules']=$this->AuthGroup->getGroupRuleIds($group_id);

        $data['rules']=list_to_tree($rules,'id','pid');
        
        $this->assign('data',$data);
      
        $this->show('wdminpage/authmanage/access.tpl');
    }


    /**
     * 用户分配列表
     * @param  int $group_id 组ID
     * @return mixed
     */
    public function allotGroupUser($group_id)
    {   
        $data['users']=\app\admin\model\AuthGroupAccess::getGroupUsers($group_id);
        //p($data);
        return $this->fetch('user',$data);
    }


    /**
     * 添加用户到组
     */
    public function addUserToGroup()
    {   
        $Model=new \app\admin\model\AuthGroupAccess();

        if($Model->addToGroup()){
            return $this->success('操作成功!');
        }else{
            return $this->error($Model->getError);
        }
    }



    public function deleteGroupUser($uid,$group_id)
    {
        if(\app\admin\model\AuthGroupAccess::deleteGroupUser()){
            return $this->success('删除成功！');
        }else{
            return $thid->error('操作失败!');
        }
    }


    public function changeStatus($group_id)
    {
        return $this->switchStatus('auth_group',['id'=>$group_id]);
    }


}



?>