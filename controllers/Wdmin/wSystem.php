<?php

/**
 * 系统控制器
 */
class wSystem extends ControllerAdmin
{

    const TPL = './views/wdminpage/';

    public function logs() {
        $this->show(self::TPL . 'system/logs.tpl');
    }

    /**
     *系统日志
     */
    public function getSystemLogs() {
        $pagesize = $this->pGet('pagesize') ? intval($this->pGet('pagesize')) : 30;
        $page     = $this->pGet('page');

        $list  = $this->Dao->select()->from(TABLE_LOGS)->orderby('id')->desc()->limit($page * $pagesize, $pagesize)->exec();
        $count = $this->Dao->select('count(1)')->from(TABLE_LOGS)->getOne();

        $this->echoJson([
            'list' => $list,
            'total' => intval($count)
        ]);

    }



    public function clearAllLog()
    {   
        if(db('wshop_logs')->execute('truncate table wshop_logs')!==false){
            return json(1,'清除成功！');
        }

        return json(0,'清除出错！');
    }

    /**
     * 备份列表 包括下载和删除两个动作
     * @author zsoner zsoner@wsxhr.com
     */
    public function backup()
    {   
        
        if(isAjax()){

            $type=$this->pGet('type');
            $file=$this->pGet('file');

            //删除
            if($type=='delete'){
                if(@file_exists($file)&&unlink($file)){
                    return json(1,'删除成功！');
                }
                return json(0,'删除失败！');
            }

            if($type=='down'){
                $file=pathinfo($file);
                $basename=$file['basename'];
                $file='http://'.$_SERVER['HTTP_HOST'].'/exports/backup/'.$basename;
                return json(1,$file);
            }           

            //默认获取列表
            $base_path=APP_PATH.'exports/backup/';
            !is_dir($base_path) && mkdir($base_path, 0777, true);
            $files=glob($base_path.'*.sql');

            if(!empty($files)){
                foreach ($files as $key => $file) {
                    $file_infos[]=$this->getFileStat($file);
                }
            }

            return json($file_infos);

        }

        $this->show('wdminpage/wsystem/backup.tpl');
    }

    /**
     * 新增备份
     * @author zsoner zsone@wsxhr.com
     * @return json  
     */
    public function createBackup()
    {   
        #参考信息：https://packagist.org/packages/ifsnop/mysqldump-php
        $db=config('db');
        $file=$db['db'].'_'.date('YmdHis').'.sql';
        
        try {

            $dump = new Ifsnop\Mysqldump\Mysqldump('mysql:host='.$db['host'].';dbname='.$db['db'],$db['user'],$db['pass']);
            $dump->start(APP_PATH.'exports/backup/'.$file);
            return json(1,'备份成功！');

        } catch (\Exception $e) {
            return json(0,$e->getMessage());
        }
    }




    /**
     * 获取文件信息
     * @param  string $file 文件路径
     * @return array        返回文件路径,大小，创建时间
     */
    protected function getFileStat($file)
    {
        $arr=Array();

        if(file_exists($file)){
            $arr['file_path']=str_replace('\\','/',$file);
            $arr['size']=format_bytes(filesize($file));
            $arr['file_time']=date('Y-m-d H:i:s',filemtime($file));
        }
        clearstatcache();

        return $arr;
    }

}