#分类是否启用
ALTER TABLE `product_category`
ADD COLUMN `status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否启用' AFTER `cat_order`;


#手动同步
ALTER TABLE `gmess_page`
ADD COLUMN `sync`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否同步到微信' AFTER `deleted`;

#批量设置产品分类
DELIMITER $$
CREATE  PROCEDURE changeGoodsCate(IN name varchar(255),IN cate_id int(10))
BEGIN
	declare changed int default 0;
	update products_info set product_cat=cate_id where locate(name,product_name)>0;
	set changed=ROW_COUNT();
	select changed;
END
$$
DELIMITER ;


#素材点击数目
ALTER TABLE `gmess_page`
ADD COLUMN `hits`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击数目' AFTER `deleted`;



#团购表
CREATE TABLE `group_buy` (
  `tuan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团购ID',
  `tuan_title` varchar(255) NOT NULL COMMENT '团购标题',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `tuan_picture` varchar(100) NOT NULL COMMENT '团购图片',
  `tuan_start_time` datetime NOT NULL COMMENT '活动开始时间',
  `tuan_end_time` datetime NOT NULL COMMENT '活动结束时间',
  `tuan_deposit_price` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '定金',
  `tuan_per_number` int(10) NOT NULL DEFAULT '0' COMMENT '每人限购数量',
  `tuan_send_point` int(11) NOT NULL DEFAULT '0' COMMENT '赠送积分数',
  `tuan_number` int(10) NOT NULL DEFAULT '0' COMMENT '限购数量',
  `tuan_pre_number` int(10) NOT NULL DEFAULT '0' COMMENT '虚拟购买数量',
  `tuan_desc` text NOT NULL COMMENT '团购介绍',
  `tuan_goodshow_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示商品详情',
  `tuan_now_number` int(10) NOT NULL DEFAULT '0' COMMENT '已团购数量',
  `tuan_order` int(10) NOT NULL DEFAULT '0' COMMENT '显示次序',
  `tuan_create_time` datetime NOT NULL COMMENT '团购创建时间',
  `tuan_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '团购更新时间',
  `tuan_price` decimal(10,3) DEFAULT '0.000' COMMENT '团购价',
  `tuan_bid` int(11) NOT NULL DEFAULT '0' COMMENT '团购所属品牌类目',
  `tuan_cid` int(11) NOT NULL DEFAULT '0' COMMENT '团购所属分类',
  `tuan_baoyou` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否包邮：0：不包邮；1:包邮',
  `tuan_remark` varchar(255) DEFAULT NULL COMMENT '团购简介',
  `tuan_start_code` tinyint(1) DEFAULT '0' COMMENT '是否启用验证码',
  `overdue_start_time` datetime NOT NULL COMMENT '补交余款开始时间',
  `overdue_end_time` datetime NOT NULL COMMENT '补交余款结束时间',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `is_deposit` tinyint(1) DEFAULT '0' COMMENT '是否启用担保金',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`tuan_id`),
  KEY `is_active` (`is_active`),
  KEY `sort_order` (`tuan_order`),
  KEY `tuan_start_time` (`tuan_start_time`),
  KEY `tuan_end_time` (`tuan_end_time`),
  KEY `product_id` (`product_id`),
  KEY `tuan_goodshow_status` (`tuan_goodshow_status`),
  KEY `overdue_start_time` (`overdue_start_time`),
  KEY `overdue_end_time` (`overdue_end_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='商品团购表';


CREATE TABLE `group_buy_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `tuan_id` int(10) NOT NULL DEFAULT '0' COMMENT '团购ID',
  `client_id` int(10) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `product_id` int(10) NOT NULL COMMENT '商品ID',
  `num` int(4) NOT NULL DEFAULT '0' COMMENT '购买数量。取值范围:大于零的整数',
  `remark` varchar(200) NOT NULL COMMENT '备注',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='团购日志记录表';


ALTER TABLE `gmess_page`
ADD COLUMN `wechat_id`  int(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '公众号ID' AFTER `hits`;
MODIFY COLUMN `title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `id`,
MODIFY COLUMN `content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容' AFTER `title`,
MODIFY COLUMN `desc`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述' AFTER `content`,
MODIFY COLUMN `catimg`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '封面图' AFTER `desc`,
MODIFY COLUMN `thumb_media_id`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `catimg`,
MODIFY COLUMN `content_source_url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '原文链接' AFTER `thumb_media_id`,
MODIFY COLUMN `media_id`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `content_source_url`,
CHANGE COLUMN `createtime` `create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `media_id`,
MODIFY COLUMN `category`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '素材分类' AFTER `create_time`,
MODIFY COLUMN `deleted`  tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除' AFTER `category`,
ADD COLUMN `update_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间' AFTER `wechat_id`,
ADD COLUMN `wei_url`  varchar(255) NOT NULL DEFAULT '' COMMENT '微信url' AFTER `update_time`;

