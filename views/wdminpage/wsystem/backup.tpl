{include file='../__header_v2.tpl'}
<div class="pd15" ng-controller="backup" ng-app="ngApp">
	<div class="pheader clearfix">
	        <div class="pull-right">
	            <button class="btn btn-default" ng-click="createBackup()"><i class="glyphicon glyphicon-plus"></i>新建备份</button>
	        </div>
	</div>
	{literal}
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>文件</th>
				<th>大小</th>
				<th>创建日期</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<tr ng-repeat="file in file_infos" >
			<td>{{file.file_path}}</td>
			<td>{{file.size}}</td>
			<td>{{file.file_time}}</td>
			<td>
				<a href="javascript:;" ng-click="down(file)">下载</a> <a href="javascript:;" ng-click="delete(file)">删除</a>
			</td>
		</tr>
		</tbody>
	</table>
	{/literal}

</div>
{include file='../__footer.tpl'} 
<script type="text/javascript" src="https://cdns.ycchen.cn/scripts/lobibox.min.js"></script>
<script type="text/javascript" src="/static/script/Wdmin/system/backup.js"></script>
<script type="text/javascript" src="/static/script/Wdmin/service/util_service.js"></script>