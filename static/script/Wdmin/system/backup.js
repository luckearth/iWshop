angular.module('ngApp',['Util.services']).controller('backup', function ($scope,Util) {

    $scope.file_infos=[];
    /**
     * 获取列表
     */
    $scope.getList=function() {
        $.ajax({
            url: '',
            type: 'GET',
            dataType: 'json',
            async:false
        }).success(function(data) {
            $scope.file_infos=data;
        });
    }

    $scope.getList();

    $scope.createBackup=function(){
        Util.loading();
        $.ajax({
                url: '/?/wSystem/createBackup',
                type: 'GET',
                dataType: 'json',
                async:false
            }).success(function(data) {
                Util.alert(data.msg);
                if(data.status){
                    $scope.getList();
                }
                Util.loading(false);
            });
    };
    /**
     * 下载文件
     * @param  {obj} file    [文件对象]
     * @return void
     */
    $scope.down=function(file) {
        doBackup('down',file,downfile);
    };

    //同步删除文件
    $scope.delete=function(file) {
        doBackup('delete',file,$scope.getList);
    };


    function downfile(data){
        var html='<iframe src="'+data.msg+'"></iframe>';
        $('body').append(html);
    }

    function doBackup(type,file,callback){
        $.ajax({
            url: '',
            type: 'GET',
            dataType: 'json',
            data:{type:type,file:file.file_path},
            async:false
        }).success(function(data) {
            callback(data);
        });
    }

});