<?php 
namespace Util;

/**
 * 阿里巴巴商品详情页采集工具
 * 暂时只采集标题、轮播图、产品描述
 * @author zsoner zsoner@wsxhr.com
 */
class Alibaba
{	
	const BASE_URL="uploads/alibaba";
	const PATTERN='/<img\s.*?src=[\\]?[\'|\"]([^\"\']+)[\\]?[\'|\"]/';


	public static function getThumbs($str)
	{
		$pt='/data-imgs=\'(.*)\'>/';
		preg_match_all($pt,$str, $matches);
		$images=array();
		foreach ($matches[1] as $key => $item) {
			$item=json_decode($item,true);
			$images[]=self::saveFile($item['original']);
		}
		return $images;

	}



	public static function getPageContent($url)
	{
		$curl = new \Curl\Curl();
		$curl->setopt(CURLOPT_RETURNTRANSFER, TRUE);
		$curl->setopt(CURLOPT_SSL_VERIFYPEER, FALSE);
		$curl->get($url);
		$curl->close();
		return $curl->response;
	}

	public static function getTitle($string)
	{	
		preg_match('/<h1 class="d-title">(.*)<\/h1>/',$string,$title);
		return $title[1];
	}


	public static function getDetail($res)
	{
		$url=self::getContentUrl($res);

		$content=file_get_contents($url);

		return self::switchStr($content);
	}


	public static function switchStr($str)
	{
		$map=['var offer_details={"content":"','};',"/style=.+?['|\"]/i",'alt=\"undefined\"'];
		$tg=['','','',''];
		$str=stripslashes(str_replace($map, $tg, $str));
		$content=preg_replace("/style=.+?['|\"]/i",'',$str);
		$content=preg_replace_callback(self::PATTERN,'self::replaceDetail',$content);
		return $content;
	}

	public static  function replaceDetail($matches)
	{
		$new_path=self::saveFile($matches[1]);

		return str_replace($matches[1],$new_path,$matches[0]);
	}


	public static function getContentUrl($str)
	{
		preg_match('/data-tfs-url="(.*)" /',$str,$urls);
		return $urls[1];
	}



	public static function saveFile($file)
	{
		$save_path=self::getMd5Path($file);

		return @file_put_contents($save_path,file_get_contents($file))?$save_path:'';
	}


	public static function getMd5Path($file)
	{
		$ext=pathinfo($file, PATHINFO_EXTENSION);

		$md5=md5($file);

		$md5_array=str_split(md5($file,false),4);

		for($i=0;$i<1;$i++) {
			$md5_path.=$md5_array[$i].'/';
		}

		$path=self::BASE_URL.'/'.$md5_path;

		self::mkDirs($path);

		return $path.$md5.'.'.$ext;

	}


	public static  function mkDirs($dir){

	    if(!is_dir($dir)){
	        if(!self::mkDirs(dirname($dir))){
	            return false;
	        }
	        if(!mkdir($dir,0777)){
	            return false;
	        }
	    }
	    return true;
	}

}



?>